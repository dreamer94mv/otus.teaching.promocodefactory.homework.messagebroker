using System;

namespace Otus.Teaching.Pcf.Administration.WebHost.Models
{
    public class NotifyAdminAboutPartnerManagerPromoCodeMessage
    {
        public Guid? PartnerManagerId { get; set; }
    }
}