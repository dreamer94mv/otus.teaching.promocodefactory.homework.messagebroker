using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Services;
using Otus.Teaching.Pcf.Administration.WebHost.Models;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using IConfiguration = Microsoft.Extensions.Configuration.IConfiguration;

namespace Otus.Teaching.Pcf.Administration.WebHost.Services
{
    public class PromocodeBackgroundService : BackgroundService
    {
        private readonly IServiceScopeFactory _scopeFactory;
        private readonly ConnectionFactory _connectionFactory;
        private readonly IConnection _connection;
        private readonly IModel _channel;

        public IConfiguration Configuration { get; }

        public PromocodeBackgroundService(IServiceScopeFactory scopeFactory, IConfiguration configuration)
        {
            Configuration = configuration;
            _scopeFactory = scopeFactory;
            _connectionFactory = new ConnectionFactory() { HostName = Configuration["RabbitMQ:HostName"] };
            _connection = _connectionFactory.CreateConnection();
            _channel = _connection.CreateModel();
            _channel.QueueDeclare(queue: "NotifyAdminAboutPartnerManagerPromoCode", durable: false, exclusive: false, autoDelete: false, arguments: null);
        }

        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            if (stoppingToken.IsCancellationRequested)
            {
                _channel.Dispose();
                _connection.Dispose();
                return Task.CompletedTask;
            }

            var consumer = new EventingBasicConsumer(_channel);

            consumer.Received += (model, ea) =>
            {
                var body = ea.Body.ToArray();
                var message = JsonConvert.DeserializeObject<NotifyAdminAboutPartnerManagerPromoCodeMessage>(Encoding.UTF8.GetString(body));

                if (message != null && 
                    message.PartnerManagerId.HasValue)
                {
                    Task.Run(async () =>
                    {
                        using var scope = _scopeFactory.CreateScope();
                        var service = scope.ServiceProvider.GetRequiredService<IService>();
                        await service.ExecuteAsync(message.PartnerManagerId.Value);
                    });
                };
            };

            _channel.BasicConsume(queue: "NotifyAdminAboutPartnerManagerPromoCode", autoAck: true, consumer: consumer);

            return Task.CompletedTask;
        }
    }
}