using Otus.Teaching.Pcf.ReceivingFromPartner.Integration.Dto;
using System;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.WebHost.Models
{
    public class NotifyAdminAboutPartnerManagerPromoCodeMessage
    {
        public Guid? PartnerManagerId { get; set; }
    }
}