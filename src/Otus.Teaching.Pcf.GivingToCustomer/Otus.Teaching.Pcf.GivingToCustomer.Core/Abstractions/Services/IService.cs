using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Services
{
    public interface IService
    {
        Task<Preference> ExecuteAsync(string message);
    }
}